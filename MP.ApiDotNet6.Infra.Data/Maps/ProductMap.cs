﻿using Microsoft.EntityFrameworkCore;
using MP.ApiDotNet6.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MP.ApiDotNet6.Infra.Data.Maps
{
    public class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Produto");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("Idproduto").UseIdentityColumn();
            builder.Property(c => c.Price).HasColumnName("Preco");
            builder.Property(c => c.Name).HasColumnName("Nome");
            builder.Property(c => c.CodErp).HasColumnName("Coderp");

            builder.HasMany(c => c.Purchases).WithOne(p => p.Product).HasForeignKey(c => c.ProductId);
        }
    }
}
