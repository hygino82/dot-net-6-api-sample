﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MP.ApiDotNet6.Domain.Entities;

namespace MP.ApiDotNet6.Infra.Data.Maps
{
    internal class PurchaseMap : IEntityTypeConfiguration<Purchase>
    {
        public void Configure(EntityTypeBuilder<Purchase> builder)
        {
            builder.ToTable("Compra");
            builder.HasKey(x => x.Id);

            builder.Property(c => c.Id).HasColumnName("Idcompra").UseIdentityColumn();
            builder.Property(c => c.PersonId).HasColumnName("Idpessoa");
            builder.Property(c => c.ProductId).HasColumnName("Idproduto");
            builder.Property(c => c.Date).HasColumnName("Datacompra");

            builder.HasOne(x => x.Person).WithMany(p => p.Purchases);
            builder.HasOne(x => x.Product).WithMany(p => p.Purchases);
        }
    }
}
