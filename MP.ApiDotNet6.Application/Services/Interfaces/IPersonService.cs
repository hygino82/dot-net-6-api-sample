﻿using MP.ApiDotNet6.Application.DTOs;

namespace MP.ApiDotNet6.Application.Services.Interfaces
{
    public interface IPersonService
    {
        public Task<ResultService<PersonDTO>> CreateAsync(PersonDTO personDTO);
    }
}
