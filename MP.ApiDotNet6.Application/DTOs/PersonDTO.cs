﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MP.ApiDotNet6.Application.DTOs
{
    public class PersonDTO
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Document { get; private set; }
        public string Phone { get; private set; }

        public PersonDTO(int id, string name, string document, string phone)
        {
            Id = id;
            Name = name;
            Document = document;
            Phone = phone;
        }
    }
}
